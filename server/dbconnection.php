<?php

// hostname of the server to connect to hosting the database
$hostName = "localhost";
// root username to gain all privalidges
$userName = "root";
// root user doesnt require a password, this is where user password would be entered
$passwd = "";
// name of the database that we want to interact with
$dbName = "datastore";

// connect to the database using the previously set variables
$conn = new mysqli($hostName, $userName, $passwd, $dbName);

// check the connection, if it fails post error message
if(!$conn){
  die("Connection failed: " . mysqli_connect_error());
} else {
  echo "Connection successful";
}

?>
