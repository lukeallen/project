$(document).ready(function(){
  console.log('ready.');

  // when the element with id addNote is clicked get text from the user and add a new note
  $('#addNote').on('click', function(){
    // debugging to check that the click listener is working and that the function is executed
    console.log('clicked');

    // collect user input
    text = prompt('enter something');
    console.log(text);

    // client side add note
    $('#notesContainer').append('<li class="collection-item">' + '<div>' + text + '</div>' + '</li>');
  });


});
